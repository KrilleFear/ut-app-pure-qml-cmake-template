import QtQuick 2.7
import QtTest 1.2
import "../../qml/"

TestCase {
    id: testCase
    name: "MainTest"

    Main {
        id: main
    }

    // Get benchmarks for Main.qml.
    function benchmark_createComponent() {
        var component = Qt.createComponent("../../qml/Main.qml")
        var obj = component.createObject(testCase)
        obj.destroy()
        component.destroy()
    }

    // Check if the hello world label exists and has the correct text.
    function test_helloWorldLabel() {
        var helloWorldLabel = findChild(main, "HelloWorldLabel")
        if (helloWorldLabel == null) fail("HelloWorldLabel was not found")
        compare(helloWorldLabel.text, 'Hello World!')
    }
}
