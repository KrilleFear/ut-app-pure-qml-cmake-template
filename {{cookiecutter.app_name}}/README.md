# {{cookiecutter.title}}

{{cookiecutter.description}}

## Tests
Run qml tests with:
```bash
clickable test
```
See [Qml Testcase](https://doc.qt.io/qt-5/qml-qttest-testcase.html) for documentation.
